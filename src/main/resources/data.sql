create table STUDENT_DATA (
                              id VARCHAR(40),
                              first_name VARCHAR(50),
                              last_name VARCHAR(50),
                              email VARCHAR(50),
                              gender VARCHAR(50)
);

insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('fc27df22-4906-420e-ac9b-ad1ff0963077', 'Neall', 'Beckwith', 'nbeckwith0@hibu.com', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('cbfc89be-c729-4458-b4bf-d611e0e3e4fe', 'Art', 'Ackery', 'aackery1@chron.com', 'Genderfluid');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('cbaa1bc1-56a9-4ea7-ad10-13a7335c6089', 'Georgiana', 'Ferrier', 'gferrier2@tumblr.com', 'Polygender');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('dc8c0854-cca3-440a-bb77-16d4ce5d1ded', 'Phillie', 'Vanyard', 'pvanyard3@dedecms.com', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('ff7065b5-6577-4d1d-95b4-89057afab73d', 'Nicola', 'Crowcroft', 'ncrowcroft4@altervista.org', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('7b166210-f41a-427d-a3df-5fa5e6ca066d', 'Norris', 'McCole', 'nmccole5@blogspot.com', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('6862ec79-f199-49d0-a8a2-9d3e8f6baf90', 'Hannie', 'Mateiko', 'hmateiko6@google.cn', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('d478cc6b-23c7-41b1-9441-28c66e0ecbf4', 'Orland', 'Want', 'owant7@pbs.org', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('f2c0a518-f2ca-4e84-8fd8-2ba943a2e298', 'Saul', 'Slopier', 'sslopier8@ucla.edu', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('13b0ab4f-19f0-4460-8c2a-eb16652fa803', 'Tarrance', 'Yukhtin', 'tyukhtin9@miitbeian.gov.cn', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('d810fd69-706c-4506-a115-9636fd14ca80', 'Zia', 'Boyd', 'zboyda@newsvine.com', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('064d9b98-6f54-4d64-b1ed-a0195b84eea6', 'Sunny', 'Gorham', 'sgorhamb@trellian.com', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('8deb9b82-a912-4992-8858-5afda6ed8805', 'Dominga', 'Trevance', 'dtrevancec@drupal.org', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('95d04662-ad9e-4ad6-962b-979906b44ce2', 'Marcelline', 'Coppin', 'mcoppind@xinhuanet.com', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('8eaed66b-6a28-4e56-bdfb-9a4ab440e79b', 'Cassandre', 'Dennerly', 'cdennerlye@yale.edu', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('a67776ad-fd43-4808-a9f0-32e128313e10', 'Ruperta', 'Haughian', 'rhaughianf@amazon.co.jp', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('3f921448-7c42-487b-845e-b5dfdfcf1587', 'Jemima', 'Grannell', 'jgrannellg@bing.com', 'Female');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('79a74add-f853-4fca-a61f-ee4dc99a846c', 'Den', 'Bister', 'dbisterh@vkontakte.ru', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('51e92290-3d91-4077-a227-9ab6aeef37fd', 'Angeli', 'Diperaus', 'adiperausi@fda.gov', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('080ba631-aec2-4d00-90ea-ffbc1629f7df', 'Phillipe', 'Kellough', 'pkelloughj@creativecommons.org', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('d2842df2-b89c-4c1b-99d6-f9b3530f6712', 'Ruddy', 'Melmeth', 'rmelmethk@sogou.com', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('c009db8b-923d-4aeb-a0b1-4646c8eb82ba', 'Noby', 'Beagin', 'nbeaginl@yandex.ru', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('bc21bca3-d18e-4c7f-bdf2-a5d835543a7b', 'Powell', 'Hodgins', 'phodginsm@feedburner.com', 'Non-binary');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('94dc3ae3-5e50-4295-b674-6236efea0087', 'Cammy', 'Dedman', 'cdedmann@is.gd', 'Male');
insert into STUDENT_DATA (id, first_name, last_name, email, gender) values ('924d091a-6950-40fa-93ca-015ea8630e2d', 'Ozzie', 'Templeton', 'otempletono@washington.edu', 'Male');