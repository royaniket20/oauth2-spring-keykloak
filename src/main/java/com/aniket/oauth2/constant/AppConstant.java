package com.aniket.oauth2.constant;

public class AppConstant {

    public static final String STUDENT_DATA = "STUDENT_DATA";
    public static final String ROLE_PREFIX = "ROLE_";
    public static final String DOT_SYMBOL_WITH_ESCAPE = "\\.";
    public static final String COMMA_SYMBOL = ",";
    public static final String CLIENT_ID = "client_id";
    public static final String GRANT_TYPE = "grant_type";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String KEYCLOAK = "keycloak";
}
