package com.aniket.oauth2.entity;


import com.aniket.oauth2.constant.AppConstant;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

/**
 * create table STUDENT_DATA (
 * 	id VARCHAR(50) PRIMARY KEY,
 * 	first_name VARCHAR(50),
 * 	last_name VARCHAR(50),
 * 	email VARCHAR(50),
 * 	gender VARCHAR(50)
 * );
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = AppConstant.STUDENT_DATA)
public class StudentEntity {


    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column
    private String id;
    @Column(name = "first_name")
    private String firstName;
    @Column(name = "last_name")
    private String lastName;
    @Column(name = "email")
    private String emailId;
    @Column(name = "gender")
    private String gender;


}
