package com.aniket.oauth2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@SpringBootApplication
@Controller
@EnableJpaRepositories
@EnableWebSecurity
public class Oauth2SpringBootKeykloakApplication {

	public static void main(String[] args) {
		SpringApplication.run(Oauth2SpringBootKeykloakApplication.class, args);
	}


	@RequestMapping("/")
	public String index() {
		return "redirect:swagger-ui/index.html";
	}
}

