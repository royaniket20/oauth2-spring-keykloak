package com.aniket.oauth2.repository;


import com.aniket.oauth2.entity.StudentEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends CrudRepository<StudentEntity,String> {
}
