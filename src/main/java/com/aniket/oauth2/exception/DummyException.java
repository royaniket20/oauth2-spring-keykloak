package com.aniket.oauth2.exception;

public class DummyException extends  RuntimeException{
    public DummyException(String message) {
        super(message);
    }
}
