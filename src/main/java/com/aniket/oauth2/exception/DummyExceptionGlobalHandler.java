package com.aniket.oauth2.exception;

import com.aniket.oauth2.dto.UniversalResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class DummyExceptionGlobalHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = DummyException.class)
    public ResponseEntity<Object> exception(DummyException exception) {
        return new ResponseEntity<>(UniversalResponse.builder()
                .message(exception.getMessage())
                .date(new Date()).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
