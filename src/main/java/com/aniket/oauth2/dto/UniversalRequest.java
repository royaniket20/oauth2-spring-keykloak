package com.aniket.oauth2.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class UniversalRequest {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Date date;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String accessToken;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String refreshToken;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String idToken;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String logoutUri;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String loginUri;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<Student> protectedResourcesWithoutRole_1_Data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String protectedResourcesWithoutRole_1_url;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Student protectedResourcesWithRole_2_Data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String protectedResourcesWithRole_2_url;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Student protectedResourcesWithRoleUnknown_3_Data;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String protectedResourcesWithRoleUnknown_3_url;

}
