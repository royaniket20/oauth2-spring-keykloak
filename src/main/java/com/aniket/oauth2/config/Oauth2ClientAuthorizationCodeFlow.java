package com.aniket.oauth2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.client.oidc.web.logout.OidcClientInitiatedLogoutSuccessHandler;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Slf4j
public class Oauth2ClientAuthorizationCodeFlow {


    @Autowired
    private ClientRegistrationRepository clientRegistrationRepository;

    @Bean
    @Order(200)
    public SecurityFilterChain configureFrontend(HttpSecurity httpSecurity) throws Exception {

        httpSecurity.csrf().disable();
        httpSecurity.securityMatcher ("/oauth2/**","/login/**","/logout","/client/**");
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/client/login").authenticated());
        httpSecurity.authorizeHttpRequests(auth-> auth.requestMatchers("/oauth2/**" , "/login/**","/logout" , "/client/**" ).permitAll());
        httpSecurity.authorizeHttpRequests(auth->auth.anyRequest().authenticated());
        httpSecurity.oauth2Login(Customizer.withDefaults());
        httpSecurity
                .logout()
                .logoutSuccessHandler(oidcLogoutSuccessHandler());
        return httpSecurity.build();
    }
    OidcClientInitiatedLogoutSuccessHandler oidcLogoutSuccessHandler() {
        OidcClientInitiatedLogoutSuccessHandler successHandler = new OidcClientInitiatedLogoutSuccessHandler(clientRegistrationRepository);
        successHandler.setPostLogoutRedirectUri("{baseScheme}://{baseHost}{basePort}{basePath}/client/logout");
        return successHandler;
    }

}
