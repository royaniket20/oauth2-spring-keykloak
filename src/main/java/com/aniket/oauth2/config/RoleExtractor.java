package com.aniket.oauth2.config;


import com.aniket.oauth2.constant.AppConstant;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class RoleExtractor implements Converter<Jwt, Collection<GrantedAuthority>> {

    private String jwtRolesPath;
    public RoleExtractor(String jwtRolesPath) {
        this.jwtRolesPath=jwtRolesPath;
    }

    @Override
    public Collection<GrantedAuthority> convert(Jwt source) {
        List<String> pathTokens = StringUtils.isEmpty(jwtRolesPath)? new ArrayList<>(): Arrays.asList(jwtRolesPath.split(AppConstant.DOT_SYMBOL_WITH_ESCAPE));
        List<String> finalRoles = new ArrayList<>();
        if(!CollectionUtils.isEmpty(pathTokens)){
            Map<String,Object> temp = null;
            for (int itr = 0 ; itr<pathTokens.size()-1;itr++){
                temp = (Map<String, Object>) (temp==null?source.getClaims().get(pathTokens.get(itr)) :temp.get(pathTokens.get(itr)));
            }
            List<String> roles = (List<String>) temp.get(pathTokens.get(pathTokens.size()-1));
            if(!CollectionUtils.isEmpty(roles)){
                finalRoles.addAll(roles);
            }
        }
        Collection<GrantedAuthority> grantedAuthorities = finalRoles.stream().map(role-> AppConstant.ROLE_PREFIX +role)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
        return grantedAuthorities;
    }


}
