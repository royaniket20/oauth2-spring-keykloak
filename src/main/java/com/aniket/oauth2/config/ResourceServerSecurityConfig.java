package com.aniket.oauth2.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

@Configuration
@EnableWebSecurity
public class ResourceServerSecurityConfig {
    @Value("${solution.jwt.roles.path}")
    private String jwtRolesPath;

    @Bean
    @Order(300)
    public SecurityFilterChain configureBackendNormal(HttpSecurity httpSecurity) throws Exception {
        JwtAuthenticationConverter    jwtAuthenticationConverter = new JwtAuthenticationConverter();
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(new RoleExtractor(jwtRolesPath));
         httpSecurity.csrf().disable();
        httpSecurity.securityMatcher ("/dummy/api/**","/v3/api-docs/**" ,"/v3/api-docs","/swagger-ui/**","/actuator/**","/maintenance/**");
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/maintenance/**","/v3/api-docs/**","/swagger-ui/**" ,"/v3/api-docs","/actuator/**").permitAll());
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/dummy/api/student").authenticated());
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/dummy/api/student/health/**").hasRole("dummy_role"));
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/dummy/api/student/id/**").hasRole("view-profile"));
        httpSecurity.authorizeHttpRequests(auth->auth.anyRequest().authenticated());
        httpSecurity.oauth2ResourceServer().jwt().jwtAuthenticationConverter(jwtAuthenticationConverter);
        httpSecurity.exceptionHandling().accessDeniedHandler(new AccessDeniedHandlerImpl());
        return httpSecurity.build();
    }
}
