package com.aniket.oauth2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@Slf4j
public class DefaultApplicationSecurityConfig {

    @Bean
    @Order(500)
    public SecurityFilterChain configureAppDefault(HttpSecurity httpSecurity) throws Exception {
         httpSecurity.csrf().disable();
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/").permitAll());
        httpSecurity.authorizeHttpRequests(auth->auth.anyRequest().authenticated());
        return httpSecurity.build();
    }
}
