package com.aniket.oauth2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.access.AccessDeniedHandlerImpl;

@Configuration
@Slf4j
public class CriticalResourceServerSecurityConfig {
    @Value("${solution.jwt.roles.path}")
    private String jwtRolesPath;

    @Value("${spring.security.oauth2.resourceserver.jwt.jwk-set-uri}")
    private String jwkUri;
    @Bean
    @Order(400)
    public SecurityFilterChain configureBackendCritical(HttpSecurity httpSecurity) throws Exception {
        httpSecurity.csrf().disable();
        httpSecurity.securityMatcher ("/critical/api/**");
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/critical/api/student").authenticated());
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/critical/api/student/health/**").hasRole("manage-account"));
        httpSecurity.authorizeHttpRequests(auth->auth.requestMatchers("/critical/api/student/id/**").hasRole("view-profile"));
        httpSecurity.authorizeHttpRequests(auth->auth.anyRequest().authenticated());
        httpSecurity.oauth2ResourceServer().opaqueToken().authenticationConverter(new CustomOpaqueTokenAuthenticationConverter(jwtRolesPath , jwkUri));
        httpSecurity.exceptionHandling().accessDeniedHandler(new AccessDeniedHandlerImpl());
        return httpSecurity.build();
    }





}
