package com.aniket.oauth2.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticatedPrincipal;
import org.springframework.security.oauth2.core.OAuth2TokenIntrospectionClaimNames;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.server.resource.authentication.BearerTokenAuthentication;
import org.springframework.security.oauth2.server.resource.introspection.OpaqueTokenAuthenticationConverter;

import java.util.Collection;

@Slf4j
public class CustomOpaqueTokenAuthenticationConverter implements OpaqueTokenAuthenticationConverter {

    private String jwtRolesPath;
    private String jwkUri;
    public CustomOpaqueTokenAuthenticationConverter(String jwtRolesPath , String jwkUri) {
        this.jwtRolesPath = jwtRolesPath;
        this.jwkUri=jwkUri;
    }

    @Override
    public Authentication convert(String introspectedToken, OAuth2AuthenticatedPrincipal authenticatedPrincipal) {
  log.info("Introspected Token - {}",introspectedToken);
        JwtDecoder jwtDecoder =  NimbusJwtDecoder.withJwkSetUri(jwkUri).build();
        Jwt token = jwtDecoder.decode(introspectedToken);
        RoleExtractor roleExtractor = new RoleExtractor(jwtRolesPath);
            Collection<GrantedAuthority> grantedAuthorities = roleExtractor.convert(token);
            log.info("Granted Authorities for Critical Resource - {}",grantedAuthorities);
            return new BearerTokenAuthentication(
                    authenticatedPrincipal,
                    new OAuth2AccessToken(OAuth2AccessToken.TokenType.BEARER, introspectedToken,
                            authenticatedPrincipal.getAttribute(OAuth2TokenIntrospectionClaimNames.IAT),
                            authenticatedPrincipal.getAttribute(OAuth2TokenIntrospectionClaimNames.EXP)), grantedAuthorities);

    }
}
