package com.aniket.oauth2.controller;


import com.aniket.oauth2.dto.UniversalResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

@RestController
@RequestMapping("/maintenance")
public class HealthController {

    @GetMapping("/health")
    public UniversalResponse getHealthData(){
        return  UniversalResponse.builder().message("App Running Correctly in Dev Mode ")
                .date(new Date()).build();
    }


}
