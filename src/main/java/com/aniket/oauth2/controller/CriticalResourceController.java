package com.aniket.oauth2.controller;

import com.aniket.oauth2.dto.Student;
import com.aniket.oauth2.dto.UniversalResponse;
import com.aniket.oauth2.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/critical/api")
public class CriticalResourceController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/student")
    public List<Student> getStudents() {
        return studentService.getStudents();
    }

    @GetMapping("/student/id/{id}")
    public Student getStudents(@PathVariable String id) {
        return studentService.getStudentById(id);
    }

    @GetMapping("/student/health/{id}")
    public UniversalResponse getStudentsHealth(@PathVariable String id) {
        UniversalResponse healthCheck = new UniversalResponse();
        healthCheck.setStudent(studentService.getStudentById(id));
        healthCheck.setMessage("Student is healthy");
        healthCheck.setDate(new Date());
        return healthCheck;
    }


}
