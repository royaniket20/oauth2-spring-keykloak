package com.aniket.oauth2.controller;

import com.aniket.oauth2.constant.AppConstant;
import com.aniket.oauth2.dto.RefreshTokenResponse;
import com.aniket.oauth2.dto.Student;
import com.aniket.oauth2.dto.UniversalRequest;
import com.aniket.oauth2.dto.UniversalResponse;
import com.aniket.oauth2.exception.DummyException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.oidc.user.DefaultOidcUser;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static com.aniket.oauth2.constant.AppConstant.REFRESH_TOKEN;


@Slf4j
@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private OAuth2AuthorizedClientService oAuth2AuthorizedClientService;

    @Autowired
    private WebClient webClient;

    @Autowired
    private Environment environment;

    @Value("${solution.oauth2.refresh-token-uri}")
    private String tokenUrlEnd;

    @Value("${spring.security.oauth2.client.provider.keycloak.issuer-uri}")
    private String tokenUrlBase;

    @Value("${spring.security.oauth2.client.registration.solution-login-client.client-id}")
    private String clientId;

    @Value("${spring.security.oauth2.client.registration.solution-login-client.client-secret}")
    private String clientSecret;

    @GetMapping("/login")
    public UniversalResponse getOauthTokens(HttpServletRequest request){
       try {
           Authentication authentication =
                   SecurityContextHolder
                           .getContext()
                           .getAuthentication();
           OAuth2AuthenticationToken oauthToken =
                   (OAuth2AuthenticationToken) authentication;
           OAuth2AuthorizedClient client =
                   oAuth2AuthorizedClientService.loadAuthorizedClient(
                           oauthToken.getAuthorizedClientRegistrationId(),
                           oauthToken.getName());
           String accessToken = client.getAccessToken().getTokenValue();
           log.info("Access token --- {}", accessToken);
           String idToken = ((DefaultOidcUser) oauthToken.getPrincipal()).getIdToken().getTokenValue();
           String refreshToken = client.getRefreshToken().getTokenValue();
           log.info("Refresh token --- {}", refreshToken);
           log.info("Auth - {}", authentication);
           String logoutUri = System.getenv().getOrDefault("BASE_URL","http://localhost:8080")+"/logout";
           return UniversalResponse.builder()
                   .message("Access Token Generated for the Principal - " + authentication.getPrincipal().toString())
                   .date(new Date())
                   .accessToken(accessToken)
                   .refreshToken(refreshToken)
                   .idToken(idToken)
                   .logoutUri(logoutUri)
                   .build();
       }catch (RuntimeException exception){
           new SecurityContextLogoutHandler().logout(request, null, null);
           throw new DummyException("Token is Invalid");
       }
    }

    @GetMapping("/logout")
    public UniversalResponse logoutFromOauth(HttpServletRequest httpServletRequest){
        String login = System.getenv().getOrDefault("BASE_URL","http://localhost:8080")+"/client/login";
            return UniversalResponse.builder()
                    .loginUri(login)
                    .message("Logged Out from system")
                    .date(new Date())
                    .build();
    }

    @PostMapping("/renew_access")
    public UniversalResponse getFreshAccessToken(@RequestBody UniversalRequest universalRequest){
        String refreshToken = universalRequest.getRefreshToken();
        WebClient client = WebClient.builder()
                .baseUrl(tokenUrlBase)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_FORM_URLENCODED_VALUE)
                .build();
        RefreshTokenResponse refreshTokenResponse =  client.post()
                .uri(tokenUrlEnd)
                        .body(BodyInserters
                                .fromFormData(AppConstant.CLIENT_ID, clientId)
                                .with(AppConstant.GRANT_TYPE,REFRESH_TOKEN)
                                .with(AppConstant.CLIENT_SECRET, clientSecret)
                                .with(REFRESH_TOKEN, refreshToken)
                        )
                        .retrieve()
                        .bodyToMono(RefreshTokenResponse.class)
                        .onErrorMap(e -> new DummyException(e.getMessage()))
                        .block();
        return UniversalResponse.builder()
                .message("New Access token & Refresh Token Retrieved from Auth Server")
                .date(new Date())
                .accessToken(refreshTokenResponse.getAccess_token())
                .refreshToken(refreshTokenResponse.getRefresh_token())
                .build();
    }

    @GetMapping("/internal/confidential")
    public UniversalResponse clientCredentialsFlow(HttpServletRequest httpServletRequest)  {
       try {
           //*********** Calling a Non role endpoint - But Protected -------------
           String baseUrl =System.getenv().getOrDefault("BASE_URL","http://localhost:8080");
           if(Arrays.asList(environment.getActiveProfiles()).contains("test")){
               baseUrl = System.getProperty("BASE_URL");
           }
               String callingUrl = baseUrl+"/dummy/api/student";
           String body = webClient.get()
                   .uri(callingUrl)
                   .retrieve()
                   .bodyToMono(String.class)
                   .block();
           final ObjectMapper objectMapper = new ObjectMapper();
           List<Student> students = objectMapper.readValue(body, new TypeReference<List<Student>>() {
           });
           UniversalResponse.UniversalResponseBuilder universalResponseBuilder = UniversalResponse.builder()
                   .protectedResourcesWithoutRole_1_url(callingUrl)
                   .protectedResourcesWithoutRole_1_Data(students)
                   .message("Fetched Data from Protected Resource Server")
                   .date(new Date());
           //*********** Calling a  Role Protected  endpoint -------------
           String callingRoleProtectedUri = baseUrl+"/dummy/api/student/id/"+students.get(0).getId();
           Student std = webClient.get()
                   .uri(callingRoleProtectedUri)
                   .retrieve()
                   .bodyToMono(Student.class)
                   .block();
           universalResponseBuilder
                   .protectedResourcesWithRole_2_url(callingRoleProtectedUri)
                   .protectedResourcesWithRole_2_Data(std);

           //*********** Calling a  Role Protected  endpoint With Unknown Role -------------
           String callingRoleProtectedUnknownUri =baseUrl+"/dummy/api/student/health/"+students.get(0).getId();
           Student stdUnknown = webClient.get()
                   .uri(callingRoleProtectedUnknownUri)
                   .retrieve()
                   .bodyToMono(Student.class)
                   .onErrorResume(throwable -> Mono.just( Student.builder().id(throwable.getMessage()).build()))
                   .block();
           universalResponseBuilder
                   .protectedResourcesWithRoleUnknown_3_url(callingRoleProtectedUnknownUri)
                   .protectedResourcesWithRoleUnknown_3_Data(stdUnknown);
           //***** Get the currently Used Access_token
           return universalResponseBuilder.build();
       }catch (RuntimeException | JsonProcessingException exception){
           exception.printStackTrace();
           throw new DummyException("Error processing Data");
       }
    }



}
