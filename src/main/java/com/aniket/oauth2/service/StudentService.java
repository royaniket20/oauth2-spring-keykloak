package com.aniket.oauth2.service;

import com.aniket.oauth2.dto.Student;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface StudentService {

    List<Student> getStudents();

    Student getStudentById(String id);
}
