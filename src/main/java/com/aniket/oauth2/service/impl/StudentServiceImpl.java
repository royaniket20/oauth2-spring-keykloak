package com.aniket.oauth2.service.impl;


import com.aniket.oauth2.dto.Student;
import com.aniket.oauth2.entity.StudentEntity;
import com.aniket.oauth2.exception.DummyException;
import com.aniket.oauth2.repository.StudentRepository;
import com.aniket.oauth2.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Service
@Slf4j
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public List<Student> getStudents() {
        List<Student> students = new ArrayList<>();
        studentRepository.findAll().forEach(student->{
            Student std = new Student();
            BeanUtils.copyProperties(student,std);
            students.add(std);
        });
        log.info("List of students retrieved - {}",students);
         return  students;
    }

    @Override
    public Student getStudentById(String id) {
        Optional<StudentEntity> studentEntityOptional = studentRepository.findById(id);
          if(studentEntityOptional.isPresent()){
              Student std = new Student();
             BeanUtils.copyProperties(studentEntityOptional.get(),std);
             return std;
        }else{
              throw new DummyException("Student Not Found with Id - "+id);
          }
    }
}
