package com.aniket.oauth2;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Slf4j
class Oauth2SpringBootKeykloakApplicationTests {

	@Test
	void contextLoads() {
	}

	@Autowired
	private MockMvc mockMvc;


	@LocalServerPort
	private int randomPort;



	@BeforeEach
	public void setUp(){
		System.setProperty("BASE_URL" , "http://localhost:"+randomPort);
	}
	@Test
	public void getAllCriticalStudents() throws Exception {
		ResultActions perform = mockMvc.perform(MockMvcRequestBuilders
				.get("/client/internal/confidential")
				.accept(MediaType.ALL)
				.contentType(MediaType.APPLICATION_JSON));
		perform.andExpect(status().isOk());
	}

}
