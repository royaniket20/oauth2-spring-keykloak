#Builder
FROM maven:3.9.0-eclipse-temurin-17-alpine AS build
COPY . /tmp
WORKDIR /tmp
RUN mvn clean package -DskipTests
#Actual Image Config
FROM eclipse-temurin:17-jdk-alpine
VOLUME /tmp
COPY --from=build /tmp/target/*.jar app.jar
EXPOSE 8080
#ARG BASE_URL_ARG="Send  '--build-arg BASE_URL_ARG=xxxx' During Docker Build"
ARG BASE_URL_ARG=https://oauth2-spring-keykloak.onrender.com
ENV BASE_URL=$BASE_URL_ARG
ENTRYPOINT ["java","-jar","/app.jar"]




##Actual Image Config Local - provided you build app locally using ide / mvn
#FROM eclipse-temurin:17-jdk-alpine
#VOLUME /tmp
#ADD target/*.jar app.jar
#EXPOSE 8080
#ARG BASE_URL_ARG="Send  '--build-arg BASE_URL_ARG=xxxx' During Docker Build"
#ENV BASE_URL=$BASE_URL_ARG
#ENTRYPOINT ["java","-jar","/app.jar"]
#docker build --progress=plain -t testing:v1 --build-arg BASE_URL_ARG=http://192.168.0.107:9090 .
